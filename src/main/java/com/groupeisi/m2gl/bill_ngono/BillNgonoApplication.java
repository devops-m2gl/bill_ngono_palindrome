package com.groupeisi.m2gl.bill_ngono;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BillNgonoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BillNgonoApplication.class, args);
	}

}
