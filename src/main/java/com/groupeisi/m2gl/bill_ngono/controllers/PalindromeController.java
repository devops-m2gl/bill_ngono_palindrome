package com.groupeisi.m2gl.bill_ngono.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class PalindromeController {

    @PostMapping("/checkpalindrome")
    public ResponseEntity<Map<String, Boolean>> checkPalindrome(@RequestBody Map<String, String> body) {
        String wordToCheck = body.get("motAverifier");

        Map<String, Boolean> response = new HashMap<>();

        response.put("isPalindrome", isPalidrome(wordToCheck));

        HttpStatus status = response.get("isPalindrome") ? HttpStatus.ACCEPTED : HttpStatus.OK;

        return new ResponseEntity<>(response, status);
    }

    private boolean isPalidrome(String word) {
        String reverse = new StringBuilder(word).reverse().toString();

        Boolean palindrome = word.equals(reverse);

        return palindrome;
    }
}
