package com.groupeisi.m2gl.bill_ngono;

import com.groupeisi.m2gl.bill_ngono.controllers.PalindromeController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PalindromeControllerTest {
    @Autowired
    private PalindromeController controller;

    @Test
    public void testPalindrone() {
        Map<String, String> request = new HashMap<>();

        request.put("motAverifier", "kayak");

        ResponseEntity<Map<String, Boolean>> response = controller.checkPalindrome(request);

        assertTrue(response.getBody().get("isPalindrome"));
        assertEquals(202, response.getStatusCodeValue());

    }

    @Test
    public void testNotPalindrone() {
        Map<String, String> request = new HashMap<>();

        request.put("motAverifier", "m2gl");

        ResponseEntity<Map<String, Boolean>> response = controller.checkPalindrome(request);

        assertFalse(response.getBody().get("isPalindrome"));
        assertEquals(200, response.getStatusCodeValue());

    }
}